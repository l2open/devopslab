package org.l2open.gameserver;

/**
 * Game server stub.
 */
public class GameServer
{
    public static String sayHello()
    {
        String hello = "Hello from the Game Server!";
        System.out.println("#################");
        System.out.println(hello);
        System.out.println("#################");
        return hello;
    }
}
