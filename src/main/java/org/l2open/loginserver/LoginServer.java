package org.l2open.loginserver;

/**
 * Login server stub.
 */
public class LoginServer
{
    public static String sayHello()
    {
        String hello = "Hello from the Login Server!";
        System.out.println("#################");
        System.out.println(hello);
        System.out.println("#################");
        return hello;
    }
}
