package org.l2open;

/**
 * Hello world!
 */
public class Hello
{
    public static String sayHello()
    {
        String hello = "Hello GitLab CI!";
        System.out.println("#################");
        System.out.println(hello);
        System.out.println("#################");
        return hello;
    }
}
